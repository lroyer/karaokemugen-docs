+++
archetype = "chapter"
title = "Contributor Guide"
weight = 2
+++

# Contributor guide

This section is about everything related to create karaokes, from gathering information, medias, syncing lyrics, to adding them to the database.

## Table of Contents

{{% children depth="999" descriptions="true" %}}
