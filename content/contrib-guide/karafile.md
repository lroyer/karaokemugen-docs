+++
title = "Karaoke Files"
weight = 4
+++

This is important: it tells Karaoke Mugen where are the files to use and some other informations to generate its database.

Examples of metadata about your karaoke: the singer’s name, the songwriter’s one, the studio that made the video, the creation date or the language used to sing.

Here, we will see how to create or modify the file via a graphical user interface.

## User interface access

- The karaoke management user interface is in Karaoke Mugen. Run the software and on the home screen, click on the button SYSTEM PANEL (you can also access it via the go to > system panel menu)

![ass](/images/creation/System-EN.jpg)

- Once in the system tab, go to the **Karaokes** emnu then "New" to access the .kara.json creation form.

![ass](/images/creation/SystemTab-EN.jpg)

## Fill the karaoke information

You now see the form to create songs.

![ass](/images/creation/Karacreatepage-EN.jpg)

The series, as all other elements, are tags. Tags have their own [data files](../tag), that contain region variations and aliases.

You will have either checkboxes, all you have to do then is check the correct ones, or a field with autocomplete that will let you create tags if it doesn't exist.

If you need to create tags, you can, after the karaoke creation, edit tags to add additional information.

### Media file

This is your video source (or audio if it's audio only). Click on this button and grab your file to add it. You can also drag & drop it directly.

### Subtitle file

This is your *time*, your *.ass*. Click on the button and go grab it or drag & drop it.

### Parent Karaokes

By selecting a parent song, KM will copy its information to the new one, allowing you to edit what changes between the parent and child. It also defines the song as a "child" of the parent. This means users will see that the parent has several chidren versions. For example it can be all versions of the Sakura Taisen opening, or a *music video* version of an *opening*.

By selecting a parent, users will see all their versions at once.

A song can have more than one parent.

### Song titles

This is the song title for your karaoke. It can exist in several languages and have aliases. Aliases allow you to specify words not found in any other title to find your karaoke more easily in the search engine.

{{% notice tip "About aliases" %}}
It's utterly **useless** to add words in aliases which are **already** in the different titles in other languages, this will only slow down the search engine.
{{% /notice %}}

A song should only have one title, but sometimes it can have several depending on the language, like in english and japanese for example.

{{% notice note "About songs in japanese" %}}
Check out [our typography section below](#typography) on how to write a japanese title correctly

You must add the title in "Japanese" (in kana) and in "Japanese - Romaji" in romaji.

Default title language should be "Japanese - Romaji"
{{% /notice %}}

### Versions

The Versions tags let you specify a song alteration, useful for covers, full versions, etc. When you are creating your karaoke, versions will be available with checkboxes (you can check many of them).

### Series

If your song does not have a series, TV or Movie (it's just a song), you can skip this part.

![ass](/images/creation/KaracreatepageSerie-FR.png)

### Song type

Use **other** only as a last resort.

### Song number

Song numbers are usually only for japanese anime songs.

### Language(s)

The langage in which your karaoke is sung. Like with everything else there is a list.

- If the langage is unknown / made up, add **"Unknown language"**.
- If your karaoke has no singing (and that your .ass is empty), add **"No linguistic content"**.
- If you have made an *instrumental/off-vocals version* (without any actual singing), choose the langage the lyrics use.

### Singer(s)

The performer(s) of your karaoke. Like for series, there is already a list, verify that your singer(s) are not present already so not to create a dupe.

### Songwriter(s)

The author(s), composer(s), arranger(s) **and** lyricist(s) of your karaoke. As with series, a list is already here, verify that the composer(s)/arranger(s)/lyricists(s) are not already in there to avoid creating a dupe.

### Creators(s)

The entity which created the video (often animation/game studios)

### Karaoke authors

It's you! Write down your name or username to add yourself to the "Karaoke Makers" list.

### Tag(s)

- **Families** : The kind of karaoke	
- **Plateform(s)** : For video games: on which platform(s) was the game released?
- **Genre(s)** : What kind of creation is it?
- **Origin(s)** : What's the karaoke from?
- **Other(s)** : Other tags.
- **Group(s)** : This allows you to group karaokes so they are downloaded together at once with the download manager. Even if you don't add anything, KM will automatically add a tag with the decade your karaoke's from.
- **Warnings** : This allows you to specify if the song can be problematic for viewers. For example because of Photo-Sensitivity Epilepsy (PSE), Spoilers, or if it's a song for adults only.
- **Collection(s)** : Which collection the karaoke belongs to? Depending on where it comes from, its language, its type, it'll go into one collection but not another. Users might not see the song if it's in a collection they haven't enabled.

## Karaoke file validation and creation

Once all the fields are filled in, you just have to click on the big blue button `Save` to generate your kara file.

If everything went well, you should see a small notification at the top of the app.

The kara file that you’ve just created was put into your primary `karaokes` folder, and video, lyrics from the *.ass* files have been renamed according to what you put in the form and placed respectively in the `medias` and `lyrics` folders (or the first of them if you have specified several ones in your `config.yml` file).

To find them among other files, sort them by modification date in your file explorer.

If you want to modify a karaoke file, [go back to this page](../create-karaoke/editkaraoke/#modifying-metadata-for-a-karaoke-or-series)

