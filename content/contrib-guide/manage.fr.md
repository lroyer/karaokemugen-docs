+++
title = "Gestion des karaokés"
weight = 3
+++

Cette page décrit comment sont gérés les karaokés dans Karaoke Mugen, comment générer la base et s'en servir.

## Les bases

Pour fonctionner, Karaoke Mugen a besoin de karaokés, on parle alors de *base de karaokés* ou de *dépôt* pour désigner l'ensemble des fichiers média, de sous-titres et de métadonnées que vous possédez.

À ne pas confondre avec la *base de données* de Karaoke Mugen qui sont des fichiers générés par celui-ci et qui contiennent toutes ses informations importantes pour fonctionner.

## Comment est structuré un karaoké dans le logiciel

Pour lire des données et créer des listes de karaokés, Karaoke Mugen a besoin de savoir où trouver vos fichiers medias, fichiers de sous-titrage et les données autour.

Un dossier de base de karaokés contient quatre sous-dossiers, qui s'appellent par défaut :

* `karaokes` : contient des fichiers **.kara.json** qui sont les métadonnées sur vos karaokés (nous allons voir comment les créer plus tard).
* `lyrics` : contient vos fichiers **.ass**, ce sont eux qui contiennent les paroles.
* `tags` : contient vos fichiers **.tag.json** pour toutes les métadonnées que vous avez ajoutées à vos karaokés.
* `hooks` : (Optionnel) contient des automatismes utilisés lors de la création de karaokés
* `medias` : contient vos fichiers vidéos (`mp4`) ou audio (`mp3`). Il est généralement en dehors du dossier de base.

Typiquement, dans le dossier d'un dépôt vous devriez avoir :

```
app
  - repos
    - myrepo.net
      - json
        - karaokes
        - lyrics
        - tags
        - hooks
      - medias
```

Ces chemins sont configurables dans le fichier `config.yml` ou via la page des dépôts du panneau système, voir la [section configuration](../../user-guide/config) pour plus de détails. Vous pouvez par exemple mettre les médias sur un disque externe (plus gros) pendant que l'appli est lancée depuis un disque interne (plus rapide) , si tel est votre souhait.

## Le fichier .kara.json

C'est un fichier de métadonnées pour votre karaoké, il est primordial : il indique à Karaoke Mugen où sont les fichiers à utiliser ainsi que d'autres infos pour générer sa base de données.

Pour plus d'informations sur la création d'un fichier **.kara.json**, consultez [la documentation](../karafile)

## Base de données PostgreSQL

Cette base est créée à partir de vos fichiers **.kara.json**, **.ass**, **.tag.json** et médias.

Elle est utilisée par Karaoke Mugen pour pouvoir faire des recherches et accéder à de nombreuses informations sur les karaokés que vous possédez.

### Générer la base de données

La génération se fait automatiquement au lancement si des modifications ont été détectées dans les fichiers depuis la dernière fois.

Celle-ci peut prendre plusieurs minutes selon la vitesse de votre ordinateur.

## Éditer/créer des fichiers karaoké

Utilisez [la documentation](../create-karaoke) pour effectuer ces opérations.
