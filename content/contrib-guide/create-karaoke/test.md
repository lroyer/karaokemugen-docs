+++
title = "Test your karaoke"
weight = 3
+++

It would be better to check your karaoke once you created it.

1. Open your favorite video player (VLC, mpv, Media Player Classic, SMPlayer, Pot Player...) and load your video.
2. Load the subtitles by adding the `.ass` file that you’ve just created.
3. Make sure the lyrics appear at the right place on th e screen.
4. Move your lips and your tongue and expire to produce the lyrics displayed at the screen with your mouth.
    - Try to stick to the singer’s. If you have to scream, go for it.
5. *(optional)* If you've been fined for loudness by the local authorities, it means your karaoke is good.

If you are not satisfied with the result, [modify your subtitle file](../editkaraoke) and do the test again.

If the results seems good to you, you can go to the karaoke file [metatadata creation](../../karafile).

Here are things you can check if you want to make the best karaoke out there.