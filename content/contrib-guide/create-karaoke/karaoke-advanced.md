+++
title = "Advanced Aegisub"
weight = 5
+++

As you saw in [the tutorial](../karaoke), we're going to focus here on Aegisub tools for already existing subtitles.

## The style manager

You can access the style manager with **Subtitles > Style manager**. This windows will open:

![Styles](/images/creation/GestionnaireStyles.png)

Let's focus on the **active script** section. That's where you change the styles that you wish to use for your subtitles. You can edit them, copy them in **Storage**, sort them by alphabetical order / change the order, delete them or copy them to create some new ones.

The left section **Storage** is roughly your Aegisub's memory,  like a style bank. You can save styles for a later use, and click on the button *"copy in the active script"* to make them active.

{{% notice info "The -furigana styles" %}}
The "-furigana" styles are generated by automated scripts from the base styles. You must remember to delete those after you finished making your karaoke since they're useless once that's done.all.
{{% /notice %}}

### Modify a style

You may want to modifiy a style, try another color, change size...

To modify a style, select it in the style manager and click at **Modify**. You will then get to this screen:

![Style](/images/creation/EditStyle.png)

Let's go into detail:

- **Style name** : Keep the default name if you have only one style, or make it explicit depending on what you want. Like "KM-Top", "KM-choir", etc. You'll avoid confusing yourself with several styles.
- **Font** : Arial is great because it's on every OS. A bit more on the right, you may change the font size. It's useful if your subtitiles are too small... or if they're a bit too big and that they display on several lines.
- **Primary Color** : This is your style's base color. For a karaoke it's the color displayed right after the syllable's sung.
- **Secondary Color** : used only in case of a karaoke: that's the color before the syllable is sung.
- **Outline Color** : This is the color of your font's border.
- **Shadow Color** : Color for your subtitles' shadows. We don't use those in our subtitles but they aren't forbidden. *To set up with the property shadow*
- **Margins** : That's what prevents your text from being displayed too close to the edges of the screen. It's possible to change it a bit, but avoid the value 0. You can play with it to put the text anywhere on the video.
- **Alignement** : You can change the default position of your subtitle, each value between 1 and 9 means a position according to what is shown here. The anchoring of your subtitle shift a bit and your subtitle will change in consequence.
- **Outline** : You can increase or reduce the size and/or the shadow of your subtitles.
- **Miscelleanous** : You can define a global rotation or increase the distance between two characters. **Do not touch the X and Y scales.**

{{% notice warning "About readability" %}}
Be careful when changing a style and try to keep readability to a maximum.
{{% /notice %}}

### Changing color

The color changing screen is the same, no matter which kind of color you modify.

![Couleur](/images/creation/Couleur.png)

You can change the color, with the small rule and then with the spectre, or directly define the RGB setting for the color that you want to see.

Note that it is also possible to change the **"Alpha"** channel of your color, i.e its transcluscence. All the colors are defined on one byte, from 0 to 255 or from 00 to FF depending on if you use the hexadecimal definition or of the decimal one. You also have a "pipette" to pick a color from anywhere on your screen.

The ASS definitions that we'll often use are

- Orange : `&H0088EF&`
- Blue : `&HEF9900&`
- Red : `&H5555FF&`
- Green : `&H66EF22&`

{{% notice warning "About readability... again" %}}
Keep in mind that the difference between the primary and the secondary color must be sufficiently high to get a good contrast so users can clearly indentify when to sing and this even from afar or with a light reflection on the screen.

Depending on the video, it might be more relevant to put a clearer colour, so the lyrics stand out from the rest of video, if the video's background is mostly dark.
{{% /notice %}}

## The time shift

A very useful trick when subtitles seem a bit shifted compared to what is being sung on a video : the time shift. It's absolutely needed when replacing a video by another one with better quality, for example.

Go to **Timing > Time shift**. The following screen then appears:

![Temp](/images/creation/DecalageTemporel.png)

You can define the time shift duration that you'd like to apply to the subtitles as accurately at the hundreth of second, backward as foward.

Play around with time shift to see what you get via the audio spectre *in karaoke mode*. If it's not good enough, press CTRL+Z and try again with another duration.

You can apply the time shift to all of the subtitles or only to an already pre-selected span, but also apply it only to ending or start timings, to get more display time.

### Convert subtitle speed

Sometimes, it may happen that your subtitles are getting unsynced gradually during the video. That's because of the video's FPS (Frames per second) might differ if you changed videos recently.

The most common FPS in video are 25 FPS and 23.97 FPS, corresponding to the [PAL](https://en.wikipedia.org/wiki/Phase_Alternating_Line) and to the [NTSC](https://en.wikipedia.org/wiki/National_Television_System_Committee).

Follow those steps:

- **File -> Export as...**
- Tick the  **FPS transformation** box :
- In **Input's FPS :** the number of FPS of the original video (often 25 FPS)
- In **Output's FPS :** the number of FPS of the new video (often 23.97 FPS)
- Then click on **Export...** and test the new karaoke, without forgetting the time shift!

If it does not work in one way, always try the other one. If the value 25 and 23.97 do not work, and you've got no idea what the video's FPS is... you are unlucky unfortunately! You're going to have to remake the karaoke again.

![FPS](/images/creation/TransformFPS.png)

### Karaoke scripts

Aegisub has a scripting system which can help when mass-modifying stuff. These are text files with the `.lua` file extension and there are a lot fo them on the Internet. The customized Aegisub [from Japan7](../material) contains some of them in particular that can be helpful to save some precious time. Let's see what they are.

![Temp](/images/creation/scripts-lua.jpg)

{{% notice note "About Aegisub versions" %}}
If you already installed the official version of Aegisub, these scripts can be found here. Right-click on the script titles and use "Save as". Place them in your `Aegisub/automation/autoload` folder
{{% /notice %}}

{{% notice tip "Tips & Tricks" %}}
You can select more than one line at a time to apply a script to all these lines at once*
{{% /notice %}}

- [Clean k tags](/aegisub/clean-k-tags.lua) : Allows you to merge useless double tags in one. For example `{\k6}{\k10}` becomes `{\k16}`

- [Nihongo wa muzukashi (wohe.lua)](/aegisub/wohe.lua): Replaces all japanese particles "o / ha / e" by "wo / wa / he".

- [Double consonants (double-consonants.lua)](/aegisub/double-consonants.lua) : Modify the k tags so that the double consonants are in the right place in Japanese.

- [Split karaoke line (karaoke-split.lua)](/aegisub/karaoke-split.lua) : Allows you to split one or more lines in two while keeping the syllables timing. For that, add a `{split}` between two words of the same line before applying the script

![Temp](/images/creation/split.jpg)

- [Mugenizer (karaoke-adjust-1sec.lua)](/aegisub/karaoke-adjust-1sec.lua) : Without a doubt the most useful script when you have an old song to edit. Applies the 0x0 resolution, adds a line to have a fading second delay, and fixes the "pre-singing splits" by readjusting the time in consequence. [(see the Syllable synchronization section here)](../karaoke)

- [auto-split](/aegisub/auto-split.lua) *(from a different source so not in the Japan7 version)* : Once you have placed the start and end times on your lines, this script can, with one click, split syllables for you, which can help you save a lot of time. **Beware**, this automation *only* works for japanese words. If you apply it to lines with one or more english words, it'll try to split them letter by letter. Don't hesitate to fix the splitting yourselves after that since it'll split `zutto` in `zu | t | to` even though you might not want to split that word in three.

- [Rubyfication](/aegisub/rubyfication.lua) *(from a different source so not in the Japan7 version)* : This script aims to help users that want to contribute to the Kana collection. It adds a special template line that, in combination with the [multi highlighting syntax](https://aegisite.vercel.app/docs/latest/furigana_karaoke/#multi-highlight-syntax), will make sure that ruby (ex: furigana in japanese) are displayed correctly when the karaoke template is applied. It also provides two variables to tweak the spacing between lines and to tweak the minimal duration for karaoke fill effect (`kf`).
