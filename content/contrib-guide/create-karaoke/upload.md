+++
title = "Sending your karaoke"
weight = 4
+++

Read the rules, terms and conditions of the repository you wish to send your songs to. If it uses Karaoke Mugen Server, a form can be filled to send them your song and participate to their community!
