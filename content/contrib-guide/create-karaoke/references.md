+++
title = "References"
weight = 9
+++

This section is relevant only if you wish to fully fill all the information on your karaoke.

## Websites

When you edit or add a song, it's important to have good references so not to make a mistake when naming artists, singers, songwriters, studios, etc.

Beware, some of them use a particular writing style (with quotes or other special characters).

{{% include "includes/inc_series-names.md" %}}

## Lyrics

{{% include "includes/inc_lyrics.md" %}}