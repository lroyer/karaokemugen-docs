+++
title = "Problèmes récurrents"
weight = 8
+++

## Les `.kara.json`

### Les outils

Les fichiers **.kara.json** doivent être tous créés et remplis automagiquement par [le formulaire](../../karafile) dans l'interface panneau système.

#### La génération de données par Karaoke Mugen

Au cas où vous modifieriez des fichiers médias ou de paroles, vous pouvez faire modifier les **.kara.json** correspondant de plusieurs façons :

- **Depuis la page d'un karaoke du panneau système** : Glissez-déplacez un fichier média ou de paroles dans les zones prévues puis cliquez sur Sauvegarder pour que Karaoke Mugen génère et ermplace le fichier **.kara.json**
- **Depuis l'interface de gestion système** : dans l'onglet *Database* cliquez sur *Valider*.
- **Au lancement de Karaoke Mugen** avec l'option `--validate` ce dernier va vérifier que vos fichiers n'ont pas changé. S'ils ont changé, ils seront mis à jour et Karaoke Mugen s'arrêtera.

## Les fichiers de paroles `.ass`

### Votre synchronisation est décalée

Voyez la section [décalage temporel du guide avancé d'Aegisub](../karaoke-advanced/#convertir-la-vitesse-des-sous-titres)

## Les médias

### Les outils

Pour faire de l'encodage vidéo, de multiples outils existent.

- **Handbrake** est le plus simple d'utilisation, avec une interface relativement lisible et des options par défaut correctes.
- **MediaCoder** est un peu plus brouillon mais regorge d'options supplémentaires : comme la possibilité de garder certains flux et réencoder d'autres. Attention, il s'agit d'un *freemium* : certaines options ne sont accessibles que via la version payante du logiciel,
- **MeGUI** est beaucoup plus complexe d'utilisation mais optimise les encodages.
- **FFMPEG** est utilisable en ligne de commande, ce qui le rend très puissant mais long à maîtriser.
- **ShotCut** permet de couper à la frame près et embarque FFMPEG, ce qui fait que vos exportations seront au poil et peu lourdes.

### Erreurs récurrentes

#### Métadonnées faussées

Il est déjà arrivé que des vidéos aient une métadonnée **duration** complètement fausses par rapport à la réalité. Si cela arrive, il est possible de régler le problème en recapsulant les flux vidéo et audio concernés dans un container MP4, en utilisant ffmpeg et la ligne de commande suivante [(voir la section dédiée)](../soft-advanced/#corriger-la-duree)

#### Paroles "écrasées" sur Karaoke Mugen

Si vous voyez que les paroles en haut de l'écran ont l'air écrasées par rapport à d'autres karaokés, ou que des lettres sont sautées lors de la progression, c'est que votre vidéo a un mauvais ratio d'indiqué dans ses métadonnées. Voir la capture ci-dessous :

![ass](/images/creation/crushedSubtitle.jpg)

Un réencodage de la vidéo, avec mise à l'échelle est alors nécessaire.

Pour trouver les bonnes valeurs, il faut soit regarder dans la console de mpv, soit regarder avec ffmpeg. Par exemple, ici avec mpv :

```
VO: [vdpau] 720x576 => 1024x576 vdpau[yuv420p]
```

Ici, mpv indique que la vidéo est étirée en largeur, passant de 720 à 1024 pixels.

Pour réencoder la vidéo, utiliser ffmpeg et la commande suivante, en remplaçant les valeurs 1024:576 par les valeurs indiquées par mpv.

```
./ffmpeg -i video_source.mp4 -c:a copy -c:v libx264 -vf "scale=1024:576" video_destination.mp4
```

Pour plus d'explications,  [(voir la section dédiée)](../soft-advanced/#corriger-le-probleme-du-ratio)