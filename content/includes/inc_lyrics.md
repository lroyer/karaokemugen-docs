* [Lyrical Nonsense](https://www.lyrical-nonsense.com/) :
Probably one of the best. Lyrics in kanjis + romajis and sometimes more. They're very active about adding new lyrics and update those after the single's released (which gives us the precious booklets containing lyrics). In order to know if one of their transciptions is an ear-made one, scroll down and you will see the *status*, "official" "transliteration" or "by ear". You will usually also get the song's release date.
* [VGMdb](https://vgmdb.net/db/main.php) : A huge database containing singles / albums / OST released to this day. You'll sometimes find booklet scans by looking on the right at the "cover" field on the song's page.
* [Anime Lyrics](https://www.animelyrics.com/)
* [Anime Song Lyrics](https://www.animesonglyrics.com/)
* [Asia Lyrics](https://www.azlyrics.com/a/asia.md)
