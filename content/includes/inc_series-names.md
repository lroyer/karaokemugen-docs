* For animation :
	* [anidb](https://anidb.net/) (_songs_ tab on an anime page, right after the  _Anime Tags_ section )
	* [MyAnimeList](https://myanimelist.net/)
	* [vgmdb](https://vgmdb.net/)
	* Series-centric wikis
	* Wikipedia (check all languages)
	* [NicoNico](https://www.nicovideo.jp/) / [BiliBili](https://www.bilibili.com/) (for AMVs)

* For video games and visual novels :
	* [vndb](https://vndb.org/)
	* Series-centric wikis
	* Wikipédia (check all languages)

* For tokusatsu and live series:
	* Series-centric wikis
	* Wikipédia (check all languages)

* And as a rule of thumb :
	* Google (!)