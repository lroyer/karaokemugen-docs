+++
archetype = "home"
title = "Karaoke Mugen Documentation"
+++

Welcome to the **Karaoke Mugen** documentation.

(Documentation en Français [disponible ici](https://docs.karaokes.moe/fr))

**Karaoke Mugen** is a karaoke management software, may it be in public with a crowd (anime conventions, Twitch streams, Discord, etc.) or in private with friends or family. 

If you're looking for an introduction to the software and its features, check [the website](http://mugen.karaokes.moe) out.

## Table of Contents

{{% children depth="999" descriptions="true" %}}
