+++
archetype = "home"
title = "Documentation de Karaoke Mugen"
+++

Bienvenue dans la documentation de [**Karaoke Mugen**](https://mugen.karaokes.moe/).

**Karaoke Mugen** est un logiciel de gestion de karaokés, utilisable lors d'évènements (conventions, livestreams *Twitch*, soirées *Discord*) ou en famille. 

Si vous cherchez à en savoir plus sur le logiciel et ses fonctionnalités, visitez [le site web](http://mugen.karaokes.moe).

## Sommaire

{{% children depth="999" descriptions="true" %}}
