+++
title = "Guide de l'opérateur de karaoké en évènement public"
weight = 8
+++

Vous organisez un karaoké avec un public que vous ne connaissez pas forcément bien ? Au hasard, en convention ? Alors ce guide est fait pour vous.

Il vise à proposer une checklist et des idées pour mieux gérer un karaoké public si vous n’y êtes pas habitué.

N’oubliez pas qu’il s’agit avant tout d’un guide et de recommandations. Adaptez selon votre situation, selon vos envies aussi. Le karaoké c’est une histoire de goût !

Bien sûr, si vous souhaitez un karaoké non participatif, où vous êtes seul maître de votre liste de lecture, certaines sections de ce guide ne vous concerneront pas.

Vous pouvez [contribuer à ce guide en éditant le fichier dans le dépôt git](https://gitlab.com/karaokemugen/sites/docs) (vous devrez vous créer un compte ou vous connecter avec votre compte Github/Google).

## Prérequis

Vérifiez bien les éléments suivants pour votre karaoké.

### Matériel

#### Un ordinateur, Mac ou PC

La configuration demandée par Karaoke Mugen est un ordinateur suffisamment puissant pour décoder de la vidéo et faire tourner une application web. Normalement, n’importe quel ordinateur moderne fera l’affaire. Néanmoins **il est recommandé de tester le logiciel chez soi, sur l'ordinateur choisi** avant l’évènement. Si possible avec un petit groupe d’amis.

#### Un (grand) écran

N’oubliez pas que vos utilisateurs devront pouvoir voir le haut de l’écran, parfois même le bas, pour certains karaokés qui affichent les choeurs en bas.

#### Des enceintes puissantes

Votre système sonore doit être adapté au lieu. Cela paraît évident, mais il est bon de le rappeler.

#### Une connexion Internet

**Si vous souhaitez que vos utilisateurs se connectent pour pouvoir suggérer des chansons, il va vous falloir vous munir d'un accès Internet.**

Depuis la version 5.0, Karaoke Mugen permet d'accèder à votre karaoké de façon distante, sans avoir besoin d'être sur le même réseau Wi-Fi. Équipez-vous d'un Internet stable (la stabilité prévaut sur la capacité de la connexion, préférez une connexion plus stable de 10Mbps à une connexion instable/incertaine de 100Mbps) et vous serez opérationnel. Si vous êtes en mesure de le faire, testez également cette connexion Internet en faisant accéder quelques uns de vos amis à qui vous pourrez envoyer le lien d'accès à distance

Une clé USB 4G fera parfaitement l'affaire, si votre opérateur a de bonnes antennes dans le lieu choisi.

#### Posters

Fabriquer une affiche pour promouvoir votre karaoké est une bonne idée : si elle est affichée quelque part de visible dans votre salle, c’est encore mieux. Indiquez l’adresse de votre instance (avec un code QR si la taille de l'affiche le permet par exemple) ou utilisez le système d'accès à distance (`https://xxx.monkmserver.net`) proposé par les options de Karaoke Mugen.

Un rappel de comment se connecter et ajouter des titres au karaoké peut mieux inciter les utilisateurs à s’amuser avec l’application.

### Logiciel

#### Mettez à jour le logiciel

Assurez-vous d’avoir la dernière version stable en date, ou une version que vous avez bien testée. Pour récupérer la dernière version, allez voir [la page de téléchargements](http://mugen.karaokes.moe/download.html).

#### Mettez à jour votre base de karaokés

La base de karaokés se met à jour toute seule au démarrage, donc lancez le logiciel la veille pour vous assurer que tout est à jour.

##### Les médias vidéo et audio

Par défaut Karaoke Mugen ne va télécharger que les médias que vous ajoutez en liste de lecture au moment où vous le faites. **Si vous êtes sans Internet le jour J ou que votre connexion est limitée (traffic ou bande passante) il vous faut tous les médias sur votre ordinateur**. Sinon vous ne pourrez pas jouer les karaokés demandés par vos utilisateurs.

Si vous avez prévu une liste de lecture fixe à l'avance, sans ouvrir l'accès au public, alors il n'y a pas de problème : assurez-vous de passer chaque playlist en "courante" une fois afin que Karaoke Mugen ajoute les chansons à télécharger. Vous pouvez vérifier l'état du téléchargement : si une chanson dans la liste indique un petit nuage à côté de son nom c'est qu'elle n'est pas encore téléchargée.

**Il est recommandé, pour une utilisation publique, d'avoir tous les médias à disposition.** Pour celà, dans le panneau système, cliquez sur l’onglet **Téléchargements des médias** et récupérez les derniers fichiers média avec le bouton **Synchroniser**.

{{% notice warning "Attention à votre espace disque" %}}

L'intégralité de la base de karaokés pèse plusieurs centaines de giga-octets. Il est préférable de dédier un disque dur entier à Karaoke Mugen pour le stockage des médias.

{{% /notice %}}

Vous pouvez changer le dossier de stockage des médias dans le panneau système, en modifiant les paramètres du dépôt.

[Consultez la documentation pour plus d’informations](../getting-started)

## Configuration

Passez en revue les éléments suivants afin de configurer votre session aux petits oignons. Nous allons voir ensemble quels paramètres sont susceptibles de vous intéresser.

Pour chaque paramètre, référez-vous à [la documentation sur la configuration](../config) pour plus d’informations.

### Paramétrage

#### Playlists

Si vous laissez les personnes ajouter des karaokés, il est impératif de [séparer la playlist courante de la playlist publique](../playlists/#listes-de-lecture), afin qu'un opérateur puisse passer en revue les suggestions et valider ou refuser celles-ci. *On n'a jamais dit que vous ne pouviez pas juste croire en votre public, mais c'est pas dit que ça soit probant.*

#### Ouverture de l’interface

L’interface publique peut être configurée en 3 états :

- **Ouvert** : Situation par défaut, les gens pourront ajouter des titres et faire des recherches ;
- **Restreint** : Seules les informations de la piste en cours et de la playlist en cours sont disponibles. Les utilisateurs ne peuvent pas interagir avec. C’est un paramètre utile si vous ne voulez pas que vos utilisateurs ajoutent quoi que ce soit à la liste, mais puissent quand même la consulter ;
- **Fermé** : Rien n’est disponible. Tout est fermé. Une page indiquera que l’interface n’est pas utilisable pour le moment.

Vérifiez bien que l’interface est dans l’état désiré avant de commencer.

#### Accès distant

L'accès distant de Karaoke Mugen peut être configuré depuis les options. Dès la première activation, un code à 4 caractères vous sera attribué. Vous aurez donc une URL dédiée du format `https://xxx.kmserver.net/` pour permettre à n'importe qui doté d'un accès à Internet de se connecter à votre session karaoké. Vous pouvez nous [demander](https://mugen.karaokes.moe/contact.html) à obtenir un code personnalisé si cela vous plaît plus. Il suffit de joindre votre jeton d'accès distant (là aussi accessible dans les paramètres) à votre demande pour un évènement particulier.

#### Quotas d’utilisation

Karaoke Mugen permet de contrôler combien de chansons vos utilisateurs peuvent ajouter durant votre karaoké.

- Soit par nombre de titres ;
- Soit par durée des musiques ;

C’est optionnel et on peut ne pas mettre de limite du tout.

Selon la durée de votre karaoké (1 heure, 4 heures, 20 heures...), vous voudrez peut-être régler cela différemment.

Dans un karaoké public, il peut être souhaitable de paramétrer cette option sur :

- 2 ou 3 chansons à la fois ;
- 4 minutes de karaoké.

Qu’est-ce que ça signifie ?

Cela veut dire que vos utilisateurs ne pourront pas, par exemple, proposer plus de 3 chansons. S’ils essaient d’en rajouter une quatrième, leur quota indiquera 0 et ils ne pourront plus proposer de nouvelles chansons.

Ce qui peut faire remonter leur quota :

- Une de leurs chansons passe à l’écran ou est acceptée par l'opérateur depuis la liste de suggestions
- Un délai de 60 minutes (valeur configurable) s’est écoulé depuis l’ajout de leur dernière proposition ;
- Un tiers (valeur configurable) des personnes connectées vote pour l’une de leurs chansons dans la liste de suggestions.

Dans le cadre d’un karaoké en mode "privé" (où tous les ajouts se font directement sur la liste courante), cela peut éviter les abus.

Dans le cadre d’un karaoké en mode "public" (où tous les ajouts se font sur la liste de suggestions), cela est moins important d’avoir un quota, mais peut ennuyer l’opérateur, car il aura alors beaucoup de suggestions parmi lesquelles faire le tri.

À vous d’activer ou non cette fonctionnalité.

#### Intervalle entre les jingles

[Les jingles de Karaoke Mugen](https://gitlab.com/karaokemugen/medias/jingles) sont joués toutes les 20 chansons (par défaut) soit environ 30 minutes de karaoké avec des génériques de 1 min 30 chacun.

Selon la durée de votre karaoké, vous voudrez peut-être mettre moins de chansons pour les voir apparaitre plus souvent.

Il existe des petits jingles d'intro (joués au début du karaoké, pour le lancer), des sponsors (similaire aux jingles), des encore (passé juste avant la dernière chanson) et des outro (pour clore le karaoké). Vous pouvez configurer tout cela.

#### Le mode Karaoké Classique

Ce mode est adapté aux concours de karaoké en convention. Lorsqu'une chanson se termine, un écran de pause est affiché avec le nom de la personne qui a demandé la prochaine chanson. Seuls l'opérateur et la personne qui a demandé la chanson peuvent lancer le karaoké dés qu'ils sont prêts.

#### Sondage / vote du public

Vous pouvez activer un mode sondage pour faire participer vos utilisateurs.

Gardez à l’esprit que tout le monde ne passe pas sa vie sur son smartphone et que cela peut ne pas être souhaitable lors d’un karaoké public.

Ce mode est utile si vous ne souhaitez pas vous occuper de la playlist courante et laisser vos utilisateurs ou le hasard décider de la prochaine chanson à passer. Des chansons seront ainsi ajoutées automatiquement au fur et à mesure que le temps passe.

Comment ça fonctionne ?

- Un vote est lancé et s’affiche sur les appareils des utilisateurs à chaque début de chanson ou durant les pauses si vous activez cela ;
- 4 (valeur configurable) karaokés sont pris au hasard depuis la liste de suggestions (celle que vos utilisateurs remplissent) :
  - Les musiques déjà dans la liste de lecture courante ne sont pas prises en compte dans la sélection ;
- Ils ont 30 secondes (valeur configurable) pour placer un vote sur un des titres proposés ;
- À la fin du temps imparti ou bien si l’on approche de la fin de la chanson en cours, le karaoké avec le plus de votes est ajouté à la liste de lecture courante :
  - En cas d’ex aequo (même à 0 votes), un titre au hasard parmi les ex aequo est ajouté à la liste courante ;
- Un nouveau vote est déclenché à la prochaine chanson ;
- Les karaokés perdants sont remis en jeu.

### Préparation

On y est presque !

#### La playlist de chauffe

Vos utilisateurs arrivent en cours de route, et probablement pas au tout début de votre session. Il peut être important de créer une playlist de chauffe : une liste de lecture contenant quelques morceaux (5 à 10 minutes) qui donnent le ton en attendant que des gens ajoutent des karaokés à la playlist. Évitez de mettre des chansons trop populaires pour permettre à votre public d’ajouter des propositions plutôt que de chanter !

N’hésitez pas à rajouter quelques chansons vous-même si vos utilisateurs sont plus occupés à chanter qu’à remplir la playlist !

#### La liste noire / les critères de liste noire

Karaoke Mugen permet de "cacher" des karaokés en empêchant leur sélection, et donc leur ajout par les utilisateurs. En ajoutant certains critères, le logiciel composera à votre place la liste noire pour que vous puissiez chanter sur vos deux oreilles (si, si).

Quelques exemples de choses à faire selon votre public :

- Ajouter le tag "R18" si vous êtes dans un évènement tout public, pour éviter que quelqu’un n’ajoute un karaoké au contenu osé ou inadapté aux yeux de tous ;
- Ajouter le tag "Spoiler" aux critères pour éviter de divulgâcher certaines séries accidentellement. Après ça dépend de ce que vous appelez un spoiler et si vous considérez le fait que Usagi c’est Sailor Moon comme étant toujours un spoiler ;
- Ajouter le tag "Epilepsie" pour filtrer les karaokés avec des visuels trop clignotants qui pourraient gêner voir causer des crises pour certaines personnes.
- Ajouter un critère de temps, par exemple pour empêcher tous les karaokés supérieurs à 2 minutes si vous n’avez pas beaucoup de temps de karaoké devant vous afin d’éviter que la liste de lecture ne soit monopolisée par des chansons longues.

À vous de voir ce que vous voulez interdire ou non. Notez qu’un karaoké présent dans la liste blanche ne sera jamais interdit, même si un critère de la liste noire pourrait l’y ajouter.

#### Dernières vérifications matérielles

- Faites un test sonore ;
- Vérifiez que tout s’affiche bien à l’écran.

## Pendant le karaoké

### Rappel utile

Si vous rencontrez des soucis de performance (les gens se plaignent que ça rame, et le logiciel a du mal à passer à la chanson suivante, etc.), fermez l’interface au public et faites passer un papier pour noter les suggestions des gens. Comme au bon vieux temps.

Si les problèmes persistent, redémarrez Karaoke Mugen.

Les développeurs sont toujours à l’écoute pour améliorer les performances et optimiser le logiciel, mais selon votre ordinateur et la situation, il peut encore y avoir des soucis. N’hésitez pas à [remonter tout souci rencontré](https://mugen.karaokes.moe/contact.html).

### Annonces

Au lancement de votre session, expliquez rapidement que les gens peuvent se connecter pour ajouter des chansons et "liker" les propositions des autres. Si vous l’avez activé, parlez du système de vote.

Notez que durant un karaoké, surtout une longue session, votre public change : certains partent et d’autres arrivent. Si vous faites des pauses, pensez à rappeler tout ça au moment opportun. Les rappels à l’écran et sur d’éventuelles affiches peuvent servir aussi.

### Organiser une playlist

Avant tout, gardez bien à l’esprit que ce ne sont que des conseils, des idées auxquelles vous n’aviez peut-être pas pensé.

Il est aussi important de s’adapter à votre public : être un bon opérateur de karaoké demande de l’observation et de l’expérience (comme un vrai DJ), c’est là où vous pouvez vous démarquer ! Selon le public de votre évènement, vous pourrez préférer des génériques français, des shonens populaires, des openings de Visual Novel ou des séries un peu plus de niches, ou uniquement du mecha.

- Considérez qu’un générique standard fait 1 min. 30. Les autres éléments (AMVs, vidéo-clips, etc.) casseront le rythme. Nous avons tous nos préférences en matière de chanson et devoir attendre plus longtemps que la moyenne pour enfin pouvoir chanter risque d’en frustrer plus d’un. Aussi, ces autres éléments sont à utiliser avec parcimonie.
  - Essayez de limiter le nombre de karaokés de plus de 2 minutes ;
  - Les séries de niche peuvent être intéressantes pour faire baisser la tension dans le public après beaucoup de karaokés populaires et lui permettre de se reposer. À doser intelligemment. Se reposer la voix c’est bien aussi de temps en temps. Cela permet également de faire découvrir des titres méconnus au public ;
  - Après un titre calme ou peu connu, passez un générique très populaire pour remettre les gens dans l’ambiance ;
  - Espacez les karaokés longs d’au moins une dizaine de minutes d’écart (7 à 10 génériques).
- Une idée qui marche parfois est d’utiliser le bouton "Mélanger" de façon régulière :
  - La liste est alors mélangée aléatoirement au-delà de la chanson en cours (les chansons déjà passées ne sont pas impactées) ;
  - Cela permet potentiellement de faire passer un karaoké récemment ajouté bien plus tôt ;
  - **ATTENTION** : certaines personnes attendent parfois que leur chanson passe avant de partir (train à prendre, envie d’aller dormir, etc.) et mélanger la playlist de façon impromptue peut en frustrer certains, car le karaoké qu’ils attendaient ne va finalement plus du tout passer au moment attendu.
  - Utilisez le mode "équilibrage" du mélangeur pour équilibrer les chansons selon leur demandeur. Ce mode crée une suite de karaokés avec toujours les mêmes demandeurs à la suite. Par exemple si vous avez des personnes A, B, et C qui ont demandé un karaoké, cela mélangera la liste en mettant un kara de A, puis de B, puis de C, puis de B, puis de C, puis de A, et ainsi de suite, toujours en faisant en sorte que les chansons de tout le monde passent..
- Incluez des karaokés en français au milieu de chansons peu populaires ou si vous voyez que le public ne chante pas autant qu’attendu. Le français, tout le monde peut en chanter, et certaines chansons sont indémodables :
  - Cat’s Eyes
  - Tortues Ninja
  - Dernière Danse (c’était une blague, reposez ces fourches)
  - Pokémon
  - Code Lyoko
  - Wakfu
  - etc.
- Attention au temps restant : surveillez souvent l’heure et prévoyez un peu de marge pour ajouter 2 ou 3 chansons **finales** :
  - Otaku no Video
  - God Knows
  - Brave Love
  - Database / Raise your flag
  - Shinzô wo Sasageyo
  - Sono Chi no Sadame
  - etc.

Si vous avez d’autres trucs et astuces de karaoké, n’hésitez pas à [contribuer à ce document](https://gitlab.com/karaokemugen/sites/docs) !
