# Karaoke Mugen documentation

This is the repository for Karaoke Mugen's documentation.

It generates [this part of the site](https://docs.karaokes.moe)

## Install

Make sure you pulled in git submodules to get the theme to work.

```sh
git submodule sync --recursive && git submodule update --init --recursive
```

You'll need [Hugo](https://gohugo.io) for this. Please download the 0.112.7 **extended** version. Any higher version is likely to error out with our current setup.

## Testing

Once installed, you can use `hugo server` and lead your browser to http://localhost:1313
