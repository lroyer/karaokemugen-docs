local tr = aegisub.gettext

script_name = tr"Rubyfication"
script_description = tr"Prepare karaoke for rubyfication"
script_author = "red5h4d0w"
script_version = "1.5.0"



function find_best_space(occupied_spaces, start_timestamp)
    local best_space = nil
    -- remove outdated lines
    for j, timestamp in ipairs(occupied_spaces) do
        if timestamp < start_timestamp - 900 then
            occupied_spaces[j] = nil
        end
    end
    -- check
    local i = 1
    repeat
        if occupied_spaces[i] == nil then
            best_space = i
            break
        end
        i = i + 1
    until false
    return best_space
end


function rubyfication(subs)
    local first = nil
    local styles_different = false
    local styles = 0
    local i_styles = {}
    local template_present = false
    -- Table of until timestamps and allocated space
    local occupied_spaces = {}


    for i, line in ipairs(subs) do
        if line.class == "info" then
            if line.key == "PlayResX" or line.key == "PlayResY" then
                line.value = "0"
            end
        end

        if line.class == "dialogue" and not line.comment and line.effect ~= "fx" then
            if first == nil then
                first = i
            end
            line.margin_t = find_best_space(occupied_spaces, line.start_time) - 1

            occupied_spaces[line.margin_t + 1] = line.end_time + 200
        end
        subs[i] = line
    end

    
    -- add ruby magic lines
    line = subs[first]
    line.comment = true
    line.start_time = 0
    line.end_time = 0
    line.margin_t = 0
    line.effect = "code once"
    line.text = "function offset() return _G.tonumber(line.margin_t)*lineOffset end"
    subs.insert(first, line)

    line.text = "lineOffset=60 -- vertical offset between lines with different vertical margins value" 
    subs.insert(first, line)

    line.effect = "template syl furi all"
    line.text = "!retime(\"line\",$lstart < 900 and -$lstart or -900,200)!{\\fade(!$lstart < 900 and $lstart or 300!,200)\\an5\\pos(!line.left+syl.center!,!$middle+offset()!)\\k!$start/10+($lstart < 900 and $lstart or 900)/10!\\!syl.tag!$skdur}"
    subs.insert(first, line)

end


aegisub.register_macro(tr"Rubyfication", tr"Prepare for multi syntax highlighting", rubyfication)