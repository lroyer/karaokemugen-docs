local tr = aegisub.gettext

script_name = tr"Double consonants"
script_description = tr"Ensure k tags are in the right place with double consonants"
script_author = "amoethyst"
script_version = "1.0"

local consonants = {"b", "c", "d", "f", "g", "h", "j", "k", "r", "m", "n", "p", "s", "t", "w", "z"}

local function _double(subs, i, letter)
    local expr = "^(.-)({\\[kK][fo]?[0-9.]+[^}]-})( -" .. letter .. ")(" .. letter .. ".*)$"
    local line = subs[i]

    local before, tag, start_tt, after = line.text:match(expr)
    while after ~= nil do
        line.text = before .. start_tt .. tag .. after
        before, tag, start_tt, after = line.text:match(expr)
    end

    return line
end


local function double(subs, sel)
    for _, i in ipairs(sel) do
        for _, c in ipairs(consonants) do
            print(c)
            local ok, res = pcall(_double, subs, i, c)
            if ok then
                subs[i] = res
            else
                aegisub.log("error on line " .. i .. ": " .. line.text .. "\n")
                aegisub.log(res .. "\n")
            end
        end
    end
end

aegisub.register_macro(script_name, script_description, double)
